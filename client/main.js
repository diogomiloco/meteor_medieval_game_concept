

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Game, Player, players } from './Game/Game';

const io_port = 3003;
let socket = require('socket.io-client')(`http://localhost:${io_port}`);

import './main.html';



socket.on('connect', function(){

  console.log('Me connected');

  var me = {
    x: Math.random()*800,
    y: Math.random()*600,
    name: "Player1"
  }

  // socket.emit('client_new_player', me);
  var my_player =  new Player(1, me.x,me.y,me.name,'me');
  players.push(my_player);
  // console.log(players);

});

// socket.on('disconnect', function(){
//   console.log('Me disconnected');
// });
//
// socket.on('player_new', function(data){
//   console.log('Player connected');
//   players.push(new Player(data.id, data.x,data.y,data.name,'enemy'));
// });
//
// socket.on('player_leave', function(){
//   console.log('Player disconnected');
// });



Template.Game.onCreated(function createGame(){
  this.canvasDimensions = new ReactiveVar({
    x: 800,
    y: 600
  })
});

Template.Game.helpers({
  canvasDimensions(){
    return Template.instance().canvasDimensions.get();
  }
});

Template.Game.events({

});

Template.Game.rendered = () => {
  var FPS = 60;
  //Game Start
  setTimeout(() => {
    var game = new Game()

    //Game Loop
    var step = function(){
      game.draw();
      window.requestAnimationFrame(step);
    }
    window.requestAnimationFrame(step);
  },1500)


};






Template.nameTest.onCreated(function nameTestCreated(){
  this.name = new ReactiveVar('abcd');
});

Template.nameTest.helpers({
  name() {
    return Template.instance().name.get();
  }
});

Template.nameTest.events({
  'input #name_change'(event, instance){
    var game = new Game();
    console.log(game.showConsoleLog());
    instance.name.set(event.target.value);
  }
});

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
});

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
});
