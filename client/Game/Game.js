var players = [];


class Npc {
  constructor(id,x,y,health, radius){
    this.context = document.getElementById('canvas').getContext('2d');
    this.canvas = document.getElementById('canvas');
    this.npc = {
      me : {
        position: {
          x: x,
          y: y
        },
        attributes: {
          name: name,
          health: health,
          mana: 100,
          speed: {x:0, y:0},
          max_speed: 8,
          acceleration:0.5,
          deceleration:1.05
        },
        weapons: [],
      },
      type:type,
    }
  }

 draw(){

 }

}

class Weapon {
  constructor(context, name,x,y){
    this.draw = {
      position: {x:300,y:300},
      size: 1,
      color: "#555599"
    }
    this.type=name;
    this.context=context;
  }
}

class MagicShield extends Weapon {
  draw_weapon(x,y, angle){
    this.context.save();
    this.context.translate(x,y);
    this.context.rotate((angle+210)*Math.PI/180);
    this.context.beginPath()
    this.context.arc(0,0,40,0, Math.PI/1.5);
    this.context.stroke();
    this.context.beginPath()
    this.context.arc(0,0,35,0, Math.PI/1.5);
    this.context.stroke();
    this.context.beginPath()
    this.context.arc(0,0,30,0, Math.PI/1.5);
    this.context.stroke();
    this.context.restore();
  }

  stop_firing(){

  }

}



class Player {
  constructor(id, x, y, name, type){
    this.context = document.getElementById('canvas').getContext('2d');
    this.canvas = document.getElementById('canvas');
    this.player = {
      me : {
        position: {
          x: x,
          y: y,
          mouseX:0,
          mouseX:0,
          aim_angle: 0
        },
        attributes: {
          name: name,
          health: 100,
          mana: 100,
          speed: {x:0, y:0},
          max_speed: 5,
          acceleration:0.25,
          deceleration:1.05,
          radius: 15
        },
        weapons: [

        ],
        equipped_weapon:0
      },
      type:type,
      isMoving: false,
      keyMap:[]
    }

    //Loads static stuff
    this.movement_handlers();
    this.loadWeapons();

  }

  loadWeapons(){
    this.player.me.weapons.push(new MagicShield(this.context,"Magic Shield", this.player.me.position.x,this.player.me.position.y));
    console.log(this.player.me.weapons);
  }


  getPosition(){
    return {x: this.player.me.position.x, y: this.player.me.position.y};
  }

  setPosition(x, y){
    this.player.me.position.x = x;
    this.player.me.position.y = y;
  }

  setId(id){
    this.id = id;
  }


  weapon_handling(){
    if (this.player.keyMap.indexOf("click") !== -1){
      this.player.me.weapons[this.player.me.equipped_weapon].draw_weapon(this.player.me.position.x,this.player.me.position.y,this.player.me.aim_angle);
    }
  }

  movement(){
      //if moving
      if (this.player.keyMap.indexOf(87) !== -1) {
        //w
        Math.abs(this.player.me.attributes.speed.y) <= this.player.me.attributes.max_speed ? this.player.me.attributes.speed.y -= this.player.me.attributes.acceleration : this.player.me.attributes.speed.y = -this.player.me.attributes.max_speed;
      }
      if (this.player.keyMap.indexOf(65) !== -1)  {
        //a
        Math.abs(this.player.me.attributes.speed.x) <= this.player.me.attributes.max_speed ? this.player.me.attributes.speed.x -= this.player.me.attributes.acceleration : this.player.me.attributes.speed.x = -this.player.me.attributes.max_speed;
      }
      if (this.player.keyMap.indexOf(83) !== -1)  {
        //s
        Math.abs(this.player.me.attributes.speed.y) <= this.player.me.attributes.max_speed ? this.player.me.attributes.speed.y += this.player.me.attributes.acceleration : this.player.me.attributes.speed.y = this.player.me.attributes.max_speed;
      }
      if (this.player.keyMap.indexOf(68) !== -1)  {
        //d
        Math.abs(this.player.me.attributes.speed.x) <= this.player.me.attributes.max_speed ? this.player.me.attributes.speed.x += this.player.me.attributes.acceleration : this.player.me.attributes.speed.x = this.player.me.attributes.max_speed;
      }

      //if not moving
      if (this.player.keyMap.indexOf( 87) === -1){
        //w
        this.player.me.attributes.speed.y = this.player.me.attributes.speed.y/this.player.me.attributes.deceleration;
      }
      if (this.player.keyMap.indexOf(65) === -1){
        //a
        this.player.me.attributes.speed.x = this.player.me.attributes.speed.x/this.player.me.attributes.deceleration;
      }
      if (this.player.keyMap.indexOf(83) === -1){
        //s
        this.player.me.attributes.speed.y = this.player.me.attributes.speed.y/this.player.me.attributes.deceleration;
      }
      if (this.player.keyMap.indexOf(68) === -1){
        //d
        this.player.me.attributes.speed.x = this.player.me.attributes.speed.x/this.player.me.attributes.deceleration;
      }
  }

  movement_handlers(){

    var keyDownHandler = (event)=>{
      if(this.player.keyMap.indexOf(event.keyCode) === -1){ this.player.keyMap.push(event.keyCode)};
      if(this.player.keyMap.length > 0) this.isMoving = true;
    }

    var keyUpHandler = (event)=>{
      if(this.player.keyMap.indexOf(event.keyCode) !== -1) this.player.keyMap.splice(this.player.keyMap.indexOf(event.keyCode),1);
      if(this.player.keyMap.length == 0) this.isMoving = false;
    }

    var mousemoveHandler = (event)=>{
      var rect = this.canvas.getBoundingClientRect();

      var hip = Math.sqrt(
        Math.pow(
          (event.clientX-this.player.me.position.x), 2) +
        Math.pow(
          (event.clientY-this.player.me.position.y), 2)
      )

      this.player.me.position.mouseX = event.clientX;
      this.player.me.position.mouseY = event.clientY;

      var calc_angle = ()=>{
        if(this.player.me.position.mouseX >= this.player.me.position.x && this.player.me.position.mouseY >= this.player.me.position.y ){
          //1º quadrante

          return Math.asin((event.clientY-this.player.me.position.y)/hip)*180/Math.PI+90;
        } else
        if(this.player.me.position.mouseX < this.player.me.position.x && this.player.me.position.mouseY >= this.player.me.position.y ){
          //4º quadrante

          return Math.asin(-(event.clientX-this.player.me.position.x)/hip)*180/Math.PI+180;
        } else
        if(this.player.me.position.mouseX < this.player.me.position.x && this.player.me.position.mouseY < this.player.me.position.y ){
          //3º quadrante

          return Math.asin(-(event.clientY-this.player.me.position.y)/hip)*180/Math.PI+270;
        } else
        if(this.player.me.position.mouseX >= this.player.me.position.x && this.player.me.position.mouseY < this.player.me.position.y ){
          //2º quadrante

          return Math.asin((event.clientX-this.player.me.position.x)/hip)*180/Math.PI;
        }
      }

      this.player.me.aim_angle = calc_angle();

    }

    var mouseDownHandler = ()=>{
      if(this.player.keyMap.indexOf("click") === -1){ this.player.keyMap.push("click")};


    }
    var mouseUpHandler = ()=>{
      if(this.player.keyMap.indexOf("click") !== -1){ this.player.keyMap.splice(this.player.keyMap.indexOf("click"),1)};
    }

    document.addEventListener('keydown', keyDownHandler, false);
    document.addEventListener('keyup', keyUpHandler, false);
    document.addEventListener('mousemove', mousemoveHandler, false);
    document.addEventListener('mousedown', mouseDownHandler, false);
    document.addEventListener('mouseup', mouseUpHandler, false);
  }

  draw_movement(){
    this.player.me.position.x+=this.player.me.attributes.speed.x;
    this.player.me.position.y+=this.player.me.attributes.speed.y;

  }

  draw_hud(){
    //draw hud
    this.context.font="20px sans serif";
    this.context.fillStyle = '#2222ff';
    this.context.fillText(this.player.me.attributes.name, this.canvas.width/2, 25)

    this.context.font="20px sans serif";
    this.context.fillStyle = '#ff0000';
    this.context.fillText(this.player.me.attributes.health + '\u2665', 25, 25)

    this.context.font="20px sans serif";
    this.context.fillStyle = '#2222ff';
    this.context.fillText('\u269D' + this.player.me.attributes.mana , this.canvas.width-60, 25)
  }

  draw_player(angle){
    this.context.beginPath();
    this.context.arc(this.player.me.position.x,this.player.me.position.y,this.player.me.attributes.radius, 0,2*Math.PI, false)
    this.context.fillStyle = "#339933";
    this.context.fill();

    this.context.save();
    this.context.translate(this.player.me.position.x,this.player.me.position.y);
    this.context.rotate(angle*Math.PI/180);
    this.context.fillStyle = "#339933";
    this.context.fillRect(-15,0,8,-15);
    this.context.fillStyle = "#339933";
    this.context.fillRect(7,0,8,-15);
    this.context.restore();
  }


  draw_enemy(){
    this.context.fillStyle = "#993333";
    this.context.fillRect(this.player.me.position.x,this.player.me.position.y,30,30);

  }

  draw(){
    this.movement();
    this.draw_movement();
    if(this.player.type ==='me'){
      this.draw_hud();
      this.weapon_handling();
      this.draw_player(this.player.me.aim_angle);

    } else if (this.player.type ==='enemy') {
      this.draw_enemy();
    }
  }
}



class Game {
  constructor(){
    this.context = document.getElementById('canvas').getContext('2d');
    this.canvas = document.getElementById('canvas');
  }
  getContext(){
    return this.context;
  }
  getCanvas(){
    return this.canvas;
  }

  drawPlayers(){
    players.forEach(function(player){
      player.draw();
    })
  }

  draw(){
    this.context.clearRect(0,0,this.canvas.width,this.canvas.height);
    this.drawPlayers();
  }

}


module.exports = {
  Game:Game,
  Player:Player,
  players:players
}
