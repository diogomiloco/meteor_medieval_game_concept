import { Meteor } from 'meteor/meteor';
import http from 'http';
import socket_io from 'socket.io';

Meteor.startup(() => {
  // code to run on server at startup
  const server = http.createServer();
  const io = socket_io(server);
  const io_port = 3003;

  var allClients = [];
  var allPlayers = [];

  io.on('connection', function(socket){

    socket.send(socket.id);

    allClients.push(socket);

    console.log("+" + allClients.length + ' clients');

    socket.on('client_new_player', function(data){
      data.id = socket.id;
      socket.broadcast.emit('player_new', allPlayers);
      allPlayers.push(data);
      console.log(allPlayers);

    })

    socket.on('disconnect', function(){
      console.log('Got disconnected');

      var socket_index = allClients.indexOf(socket);
      allClients.splice(socket_index,1);

      var playerIndex = allPlayers.find((player, index)=>{
        if(player.id === socket.id){
          return index;
        }
      })

      allPlayers.splice(playerIndex,1);

      socket.broadcast.emit('player_leave', allPlayers);
      console.log("-" + allClients.length + ' clients');
      console.log(allPlayers);

    })

  })

  try {
    server.listen(io_port)
  } catch (e) {
    console.error(e);
  }

});
